﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class Battery
  {
    public int mAh { get; set; }
    public bool IsRemovable { get; set; }
    public int MaxTalkTime { get; set; }
  }
}
