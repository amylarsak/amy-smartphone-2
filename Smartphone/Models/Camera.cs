﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class Camera
  {
    public bool HasFlash { get; set; }
    public int ResWidth { get; set; }
    public int ResHeight { get; set; }
  }
}
