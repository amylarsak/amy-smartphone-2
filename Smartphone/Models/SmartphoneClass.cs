﻿
namespace Smartphone.Models
{
  
  /// <summary>
  /// our sweet smartphone class
  /// </summary>
  public class SmartphoneClass
  {
    /// <summary>
    /// The Weight in oz
    /// </summary>
    public int Weight { get; set; }
    /// <summary>
    /// The Model of Smartphone
    /// </summary>
    public bool WaterResistance { get; set; }
    /// <summary>
    /// Smartphone Battery Property
    /// </summary>
    public Battery Battery { get; set; }
    /// <summary>
    /// Smartphone Apps Property
    /// </summary>
    public Apps Apps { get; set; }
    /// <summary>
    /// Smartphone Radio features Property
    /// </summary>
    public RadioFeatures RadioFeatures { get; set; }
    /// <summary>
    /// Smartphone Os Property
    /// </summary>
    public Os Os { get; set; }
    /// <summary>
    /// Smartphone Rear Camera Property
    /// </summary>
    public Camera RearCamera { get; set; }
    /// <summary>
    /// Smartphone Front Camera Property
    /// </summary>
    public Camera FrontCamera { get; set; }
    /// <summary>
    /// Smartphone CPU Property
    /// </summary>
    public CPU CPU { get; set; }

    /// <summary>
    /// Smartphone Chassis Property
    /// </summary>
    public Chassis Chassis { get; set; }

    /// <summary>
    /// Smartphone Screen Property
    /// </summary>
    public Screen Screen { get; set; }

    /// <summary>
    /// Smartphone RAM property.
    /// </summary>
    public Ram Ram { get; set; }

    /// <summary>
    /// The make of the smartphone.
    /// </summary>
    public string Make { get; set; }

    /// <summary>
    /// The model of the smartphone.
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// Phone's diagonal screen size in inches.
    /// </summary>
    public decimal ScreenSizeIn { get; set; }

    /// <summary>
    /// Read-only screen size in mm.
    /// </summary>
    public decimal ScreenSizeMm
    {
      get
      {
        return ScreenSizeIn * (decimal)25.4;
      }
    }
       
    /// <summary>
    /// Screen width in pixels.
    /// </summary>
    public int ScreenWidth { get; set; }

    /// <summary>
    /// Screen height in pixels.
    /// </summary>
    public int ScreenHeight { get; set; }

    /// <summary>
    /// Front camera megapixels.
    /// </summary>
    public decimal FrontCameraMegapixels { get; set; }

    /// <summary>
    /// Rear camera megapixels.
    /// </summary>
    public decimal RearCameraMegapixels { get; set; }
  }
}
