﻿using Smartphone.Models;
using Smartphone.Utilites;
using System;
using System.Collections.Generic;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
      //this is where your program starts
      //Entry Point

      //instance of smartphone class
      var sc = new SmartphoneClass();

      //set all the properties
      //sc.FrontCameraMegapixels = (decimal)1.3;
      //sc.Make = "LG";
      //sc.Model = "Nexus 5";
      //sc.RearCameraMegapixels = 8;
      //sc.ScreenHeight = 1920;
      //sc.ScreenSizeIn = (decimal)4.95;
      //sc.ScreenWidth = 1080;
      //sc.Ram.Size = (decimal)1120;


      sc.Make = "Apple";
      sc.Model = "iPhone 6";
      sc.Weight = 5;
      sc.WaterResistance = false;


      sc.Ram = new Ram();
      sc.Ram.Size = (decimal)1120;

      sc.Screen = new Screen();

      sc.Screen.ScreenType = Lookups.ScreenTypes.Lcd;
      sc.Screen.IsTouchScreen = true;
      sc.Screen.Size = (decimal)5.5;
      sc.Screen.ResWidth = (decimal)2.2;
      sc.Screen.ResHeight = (decimal)3.0;
      sc.Screen.PixelsPerInch = (int)401;


      sc.Chassis = new Chassis();
      sc.Chassis.MaterialTypes = new List<Lookups.MaterialTypes>();

        sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Aluminum);
        sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.CarbonFiber);
        sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Glass);
        sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Plastic);
        sc.Chassis.Length = (decimal)5.95;
        sc.Chassis.Width = (decimal)3.95;
        sc.Chassis.Height = (decimal)5.15;

        sc.CPU = new CPU();

        sc.CPU.Make = (string)"TSMC";
        sc.CPU.Model = (string)"A8";
        sc.CPU.ClockSpeed = (decimal)2.6;

        sc.FrontCamera = new Camera();

        sc.FrontCamera.HasFlash = true;
        sc.FrontCamera.ResWidth = (int)1080;
        sc.FrontCamera.ResHeight = (int)1920;
        
        sc.RearCamera = new Camera();

        sc.RearCamera.HasFlash = true;
        sc.RearCamera.ResWidth = (int)680;
        sc.RearCamera.ResHeight = (int)1020;

        sc.Os = new Os();
        sc.Os.Name = "iOS";
        sc.Os.Version = "8";

        sc.RadioFeatures = new RadioFeatures();

        sc.RadioFeatures.HasGPS = true;
        sc.RadioFeatures.HasWifi = true;
        sc.RadioFeatures.HasCdma = true;
        sc.RadioFeatures.HasLte = true;


        sc.Apps = new Apps();

        sc.Apps.App1 = "Instagram";
        sc.Apps.App2 = "Beats";
        sc.Apps.App3 = "Cartwheel";

        sc.Battery = new Battery();

        sc.Battery.mAh = (int)2400;
        sc.Battery.IsRemovable = true;
        sc.Battery.MaxTalkTime = (int)1440;

        


      
      //write the phone details out to the console
      //Console.WriteLine("**** My Smartphone ****");
      //Console.WriteLine();
      //Console.WriteLine("Make: {0}, Model: {1}", sc.Make, sc.Model);
      //Console.WriteLine("Screen Size: {0} in. ({1} mm)", sc.ScreenSizeIn, sc.ScreenSizeMm);
      //Console.WriteLine("Screen Resolution: {0} x {1} pixels", sc.ScreenHeight, sc.ScreenWidth);
      //Console.WriteLine("Rear Camera: {0} MP", sc.RearCameraMegapixels);
      //Console.WriteLine("Front Camera: {0} MP", sc.FrontCameraMegapixels);
      //Console.WriteLine();
      //Console.WriteLine("**** Press the AnyKey ****");
      //Console.ReadKey();

        Console.WriteLine("**** My Smartphone *****");

        Console.WriteLine("Make: {0}", sc.Make);
        Console.WriteLine("Model: {0}", sc.Model);
        Console.WriteLine("Weight: {0}", sc.Weight);
        Console.WriteLine("Is it Water Resistant: {0}", sc.WaterResistance ? "Yes" : "No");
        Console.WriteLine("Ram: {0}", sc.Ram.Size);
        Console.WriteLine("Touch Screen Enabled: {0}", sc.Screen.IsTouchScreen ? "Yes" : "No");
        Console.WriteLine("Screen Type: {0}", sc.Screen.ScreenType);
        Console.WriteLine("Screen Size: {0} in", sc.Screen.Size);
        Console.WriteLine("Screen Resolution Width: {0}", sc.Screen.ResWidth);
        Console.WriteLine("Screen Resolution Height: {0}", sc.Screen.ResHeight);
        Console.WriteLine("Screen Pixels Per Inches: {0}", sc.Screen.PixelsPerInch);
        Console.WriteLine("Chassis Length: {0} in", sc.Chassis.Length);
        Console.WriteLine("Chassis Width: {0} in", sc.Chassis.Width);
        Console.WriteLine("Chassis Height: {0} in", sc.Chassis.Height);
        Console.WriteLine("CPU Make: {0}", sc.CPU.Make);
        Console.WriteLine("CPU Model: {0}", sc.CPU.Model);
        Console.WriteLine("CPU Clockspeed: {0}", sc.CPU.ClockSpeed);
        Console.WriteLine("App: {0}", sc.Apps.App1);
        Console.WriteLine("App: {0}", sc.Apps.App2);
        Console.WriteLine("App: {0}", sc.Apps.App3);
        Console.WriteLine("Os Name: {0}", sc.Os.Name);
        Console.WriteLine("Os Version {0}", sc.Os.Version);
        Console.WriteLine("Radio Features - Has GPS: {0}", sc.RadioFeatures.HasGPS ? "Yes" : "No");
        Console.WriteLine("Radio Features - Has Wifi: {0}", sc.RadioFeatures.HasWifi ? "Yes" : "No");
        Console.WriteLine("Radio Features - Has Cdma: {0}", sc.RadioFeatures.HasCdma ? "Yes" : "No");
        Console.WriteLine("Radio Features - Has Lte: {0}", sc.RadioFeatures.HasLte ? "Yes" : "No");
        Console.WriteLine("Battery mAh: {0}", sc.Battery.mAh);
        Console.WriteLine("Battery Max Talk Time: {0}", sc.Battery.MaxTalkTime);
        Console.WriteLine("Is the Battery Removable: {0}", sc.Battery.IsRemovable ? "Yes" : "No");

        
        if (sc.FrontCamera != null)
        {
          Console.WriteLine("Front Camera Resolution Width: {0} in, Front Camera Resolution Height: {1} in", sc.FrontCamera.ResWidth, sc.FrontCamera.ResHeight);
          Console.WriteLine("Front Camera Has Flash: {0}", sc.FrontCamera.HasFlash ? "Yes" : "No");
        }
        else
        {
          Console.WriteLine("No Front Camera");
        }
        if (sc.RearCamera != null)
        {
          Console.WriteLine("Rear Camera Resolution Width: {0} in, Rear Camera Resolution Height: {1} in", sc.RearCamera.ResWidth, sc.RearCamera.ResHeight);
          Console.WriteLine("Rear Camera Has Flash: {0}", sc.RearCamera.HasFlash ? "Yes" : "No");

        }
        else
        {
          Console.WriteLine("No Rear Camera");
        }
        
        
        foreach (var material in sc.Chassis.MaterialTypes)
        {
          Console.WriteLine("Chassis Material Type: {0}", material);
        }

        Console.ReadKey();
    }
  }
}
