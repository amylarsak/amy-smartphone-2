﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Utilites
{
  public static class Lookups
  {
    public enum ScreenTypes
    {
      //Amoled Display
      Amoled = 1,
      Lcd = 2,
      Led = 3
    }

    public enum MaterialTypes
    {
      Aluminum = 1,
      CarbonFiber = 2,
      Glass = 3,
      Plastic = 4
    }
  }
}
